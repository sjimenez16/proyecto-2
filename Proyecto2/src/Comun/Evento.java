package Comun;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Evento implements Comparable<Evento>
{
	private int codigoCollar;
	private Date fechaHora;
	private int ritmoCardiaco;
	private double ubicacionALatitud;
	private double ubicacionALongitud;
	private int pasoPuerta;
	
	public Evento(int pCodigo, long fecha, int pRitmo, double pUbicacionALatitud, double pUbicacionALongitud, int pPasoPuerta) 
	{
		codigoCollar= pCodigo;
		fechaHora= new Date(fecha);
		ritmoCardiaco= pRitmo;
		ubicacionALatitud= pUbicacionALatitud;
		ubicacionALongitud= pUbicacionALongitud;
		pasoPuerta= pPasoPuerta;
	}
	
	public int getCodigoCollar(){
		return codigoCollar;
	}
	
	public Date getFechaHora(){
		return fechaHora;
	}
	
	public int getRitmoCardiaco(){
		return ritmoCardiaco;
	}
	
	public double getUbicacionALatitud(){
		return ubicacionALatitud;
	}
	
	public double getUbicacionALongitud(){
		return ubicacionALongitud;
	}
	
	/**
	 * Metodo que retorna si paso puerta
	 * @return 1 si salio, 0 si esta vacio, -1 si llego
	 */
	public int getPasoPuerta(){
		return pasoPuerta;
	}

	@Override
	public int compareTo(Evento o) {
		int ret=-1;
		if(this.getCodigoCollar()>o.getCodigoCollar())
		{
			ret=1;
		}
		else if(this.getCodigoCollar()==o.getCodigoCollar())
		{
			ret=0;
		}
		return ret;
	}
}
