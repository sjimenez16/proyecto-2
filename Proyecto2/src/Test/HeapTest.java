package Test;

import Estructuras.*;
import junit.framework.TestCase;

public class HeapTest extends TestCase {

	//Cola donde se haran las pruebas
	private Heap<Integer> cola;
	
	/**
	 * Una cola vacia
	 */
	public void setupEscenario1(){
		cola= new Heap<Integer>(false);
	}
	
	/**
	 * Cola con 10 elementos
	 */
	public void setupEscenario2(){
		cola= new Heap<Integer>(false);
		for (int i = 0; i < 10; i++) {
			cola.add(i);
		}
	}
	
	public void testIsEmpty(){
		setupEscenario1();
		assertEquals(true, cola.isEmpty());
	}
	
	public void testIsEmpty2(){
		setupEscenario2();
		assertEquals(false, cola.isEmpty());
	}
	
	public void testAdd(){
		setupEscenario1();
		cola.add(16);
		assertEquals(1, cola.size());
	}
	
	public void testAdd2(){
		setupEscenario2();
		cola.add(40);
		assertEquals(11, cola.size());
	}
	
	public void testPeek(){
		setupEscenario1();
		cola.add(1);
		int agarrado=cola.peek();
		assertNotNull(agarrado);
	}
	
	public void testPeek2(){
		setupEscenario2();
		int agarrado= cola.peek();
		assertNotNull(agarrado);
	}
	
	public void testPoll(){
		setupEscenario1();
		assertNull(cola.poll());
		assertEquals(0, cola.size());
	}
	
	public void testPoll2(){
		setupEscenario2();
		int p1= cola.poll();
		int p2= cola.poll();
		assertEquals(true, p2>p1);
		assertEquals(8, cola.size());
	}
}
