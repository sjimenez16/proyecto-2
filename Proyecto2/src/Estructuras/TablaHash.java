package Estructuras;

import java.util.Iterator;

//NOTA: SE ATIENDEN LAS COLISIONES CON LINEAL PROBING
public class TablaHash<K extends Comparable<K> ,V> implements ITablaHash<K ,V>
{
	public final static int Size= 10;
	public final static float cargaMax= 50;
	
	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash<K,V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		tabla= new NodoHash[Size];
		count= 0;
		capacidad= Size;
		factorCarga=0;
		factorCargaMax=cargaMax;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int pCapacidad, float pFactorCargaMax) {
		capacidad= pCapacidad;
		factorCargaMax= pFactorCargaMax;
		tabla= new NodoHash[Size];
		count=0;
		factorCarga=0;
	}

	public void put(K llave, V valor){
		if(valor==null){
			delete(llave);
			return;
		}
		if(calcularFactorCarga()>factorCargaMax){ //Si el % es mayor
			rehash();
		}
		int hash= hash(llave);
		while(tabla[hash]!=null){
			//Atender colision, si hay una colision, asigna el valor mas reciente a esa llave. 
			if(tabla[hash].getLlave().equals(llave)){
				tabla[hash].setValor(valor);
			}
			hash++; //Siguiente celda
			hash=(hash+1)%capacidad; //Calcular nuevo hash
		}
		tabla[hash]= new NodoHash<K, V>(llave, valor);
		count++;			
	}

	public V get(K llave){
		int hash= hash(llave);
		V ret= null;
		//Lineal probing
		while(tabla[hash]!=null){
			if(tabla[hash].getLlave().equals(llave)){
				System.out.println("encuentra");
				ret=tabla[hash].getValor();
				System.out.println(ret);
				break;
			}
			//Siguiente celda
			hash++;
			//Calcular nuevo hash
			hash= (hash+1)%capacidad;
		}
		return ret;
	}

	public V delete(K llave){
		int hash= hash(llave);
		//Lineal probing
		while(tabla[hash]!=null){
			if(tabla[hash].getLlave().equals(llave)){
				tabla[hash]=null;
				break;
			}
			hash++; // Siguiente celda
			hash=(hash+1)%capacidad;//Calcular nuevo hash
		}
		count--;
		rehash();
		return null;
	}

	public float calcularFactorCarga(){
		factorCarga= (count/capacidad)*100;
		return factorCarga;
	}
	
	public int darNumEl(){
		return count;
	}
	
	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return ((llave.hashCode())*31)%capacidad;
	}
	
	//TODO: Permita que la tabla sea dinamica
	private void rehash()
	{
		//Se guardan las variables viejas
		int capacidadv= count;
		NodoHash vieja[]= tabla;
		//Se cambia el tamaño de la tabla por el doble +1
		capacidad= capacidadv*2+1;
		//Nueva tabla
		NodoHash nuevaT[]= new NodoHash[capacidad];
		tabla= nuevaT;
		int i=0; //Contador
		NodoHash actual= vieja[i];//iterador
		boolean termino=false;
		//Se pasa la tabla vieja a la nueva
		while(actual!=null && !termino){
			nuevaT[i]= actual;
			i++;
			if(i==count){
				termino=true;
				break;				
			}
			actual= vieja[i];
		}
	}
}