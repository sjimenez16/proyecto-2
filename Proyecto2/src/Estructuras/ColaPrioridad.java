package Estructuras;

public class ColaPrioridad<Key extends Comparable<Key>> {

	private Key[] pq;
	private int N=0;
	public ColaPrioridad(int maxN){
		pq=(Key[]) new Comparable[maxN+1];
	}
	public boolean isEmpty(){
		return N==0;
	}
	public int size(){
		return N;
	}
	public void insert(Key V){
		pq[++N]=V;
		swim(N);
	}
	public Key eliminarMayor(){
		Key max=pq[1];
		cambiar(1,N--);
		pq[N+1]=null;
		sink(1);
		return max;
	}
	private boolean less(int i, int j){
		return pq[i].compareTo(pq[j])<0;
	}
	private void cambiar(int i, int j){
		Key t = pq[i];
		pq[i]=pq[j];
		pq[j]=t;
	}
	
	private void swim(int k){
		while(k>1 && less(k/2,k)){
			cambiar(k/2, k);
			k=k/2;
		}
	}
	
	private void sink(int k){
		while(2*k<=N){
			int j=2*k;
			if(j<N && less(j,j+1)) j++;
			if(!less(k,j)) break;
			cambiar(k,j);
			k=j;
		}
	}
}
