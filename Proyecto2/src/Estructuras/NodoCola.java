package Estructuras;

import java.io.Serializable;

public class NodoCola <T extends Comparable<? super T>> extends Object implements Comparable<NodoCola<T>>, Serializable{

	// -----------------------------------------------------------------
	// Constantes
	// -----------------------------------------------------------------

	/**
	* Constante para la serialización
	*/
	private static final long serialVersionUID = 1L; 

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	* El elemento
	*/
	private T elemento;

	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	/**
	* Constructor del elemento. <br>
	* <b> post: </b>Se construyó un objeto de tipo Elemento con la información especificada
	* @param elem El elemento que contendrá el objeto
	*/
	public NodoCola( T elem )
	{
	elemento = elem;
	}

	// -----------------------------------------------------------------
	// Métodos
	// -----------------------------------------------------------------

	/**
	* Retorna el elemento que contiene el objeto. <br>
	* <b> post: </b> Se retornó el elemento que contiene el objeto.
	* @return Se retornó el elemento que contiene el objeto
	*/
	public T darElemento( )
	{
	return elemento;
	}

	/**
	* Compara el elemento con el objeto especificado. <br>
	* <b> post: </b> Se retornó 0 si los objetos son iguales, 1 si el objeto actual es mayor al especificado o -1 si es menor.
	* @param elem Elemento con el que se va a realizar la comparación
	* @return 0 si los objetos son iguales, 1 si el objeto actual es mayor al especificado o -1 si es menor
	*/
	public int compareTo( NodoCola<T> elem )
	{
	return elemento.compareTo( elem.elemento );
	}

	}
