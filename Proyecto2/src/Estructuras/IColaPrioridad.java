package Estructuras;

public interface IColaPrioridad<T> {
	public void add(T elemento);
	public T peek();
	public T poll();
	public int size();
	public boolean isEmpty();
	void siftUp();
	void siftDown();
}
